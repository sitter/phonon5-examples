import QtQuick 2.0

Image {
    id: bigbutton
    property bool play: false         // play/pause property

    smooth: true
    source: "./Images/bigbuton.png"
    scale: bigiconarea.pressed?0.9:1
    
    Behavior on scale {
        NumberAnimation {  duration: 200; easing.type: Easing.InOutQuad }
    }
    Image {
        id: iconbig
        source: parent.play?"./Images/pause.png":"./Images/play.png"
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        smooth: true
    }
    MouseArea{
        id:bigiconarea
        anchors.fill: parent
        anchors.margins: -7
        onClicked:parent.play=!parent.play
    }
}

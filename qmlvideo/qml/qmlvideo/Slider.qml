import QtQuick 2.0

Item{
    id:slider
    height: 7
    property alias position: handle.x
    property int __sliderMax: slider.width-handle.width
    signal clik
    property int debugint: 450

    MouseArea{
        anchors.fill: parent
        hoverEnabled: true
        onClicked: parent.position=mouseX-3
        onReleased: { filteringFalseClicks.stop()
            filteringFalseClicks.restart()  }
    }
    Image {
        id:ii
        anchors.horizontalCenter: handle.horizontalCenter
        anchors.verticalCenter: handle.verticalCenter
        source: "./Images/sliderselect.png"
        smooth: true
        scale:handleArea.pressed?1:0.1
        
        Behavior on scale {
            NumberAnimation { easing.overshoot: 1.802; duration:handleArea.pressed?400:1200; easing.type: Easing.InOutBack }
        }
    }
    BorderImage {
        id: completed    
        source: "./Images/slide.png"
        width: handle.x+handle.width;
        border.left: 4;
        border.right: 4;
    }
    
    Image {
        id: handle
        source: "./Images/butonslide.png"
        MouseArea{
            id:handleArea
            anchors.fill: parent
            anchors.margins: -12
            hoverEnabled: true
            drag.target: handle
            drag.axis: Drag.XAxis
            drag.minimumX: 0
            drag.maximumX: slider.width-handle.width
            onReleased:{ filteringFalseClicks.stop()
                filteringFalseClicks.restart()  }
        }
    }
  Timer{
      id:filteringFalseClicks
      interval:debugint
      onTriggered: clik()
  }

}

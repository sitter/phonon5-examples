import QtQuick 2.0

Image {
    id: smallbuton
    signal stop        // play/pause property

    smooth: true
    source: "./Images/button.png"
    scale: smalliconarea.pressed?0.9:1
    
    Behavior on scale {
        NumberAnimation {  duration: 200; easing.type: Easing.InOutQuad }
    }
    Image {
        id: iconsmall
        source: "./Images/stop.png"
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        smooth: true
    }
    MouseArea{
        id:smalliconarea
        anchors.fill: parent
        anchors.margins: -7
        onClicked:stop()

    }
}

import QtQuick 2.0
import org.kde.phonon 1.0

Rectangle {
    width:640
    height: 480
    color: "black"

//    Image {
//        id: videoBase
//        source: "./Images/video.png"
//        MouseArea{
//            anchors.fill: parent
//            onClicked: {
//                base.y=390
//                closebase.restart()
    //            }
    //        }
    Video {
        id: videoBase
        anchors.fill: parent
        Component.onCompleted: {
            player.addOutput(videoBase)
//            player.play()
            player.setTickInterval(500)
            player.timeChanged.connect(setPosition)
            audio.volumeChanged.connect(volumeChanged)
        }

        MouseArea{
            anchors.fill: parent
            onClicked: {
                base.y=390
                closebase.restart()
            }
        }

        function setPosition(time) {
            console.debug("TIME")
            console.debug(time)

            var _nomProgress = time / player.totalTime
            var _position = sliderVideo.__sliderMax * _nomProgress
            console.debug(_nomProgress)
            console.debug(_position);
            sliderVideo.position = _position
        }

        function volumeChanged(volume) {
            sliderAudio.position = sliderAudio.__sliderMax * volume
        }

        MouseArea{
            anchors.fill: parent
            onClicked: {
                base.y=390
                closebase.restart()
            }
        }

        Image{
            id: base
            anchors.horizontalCenter: parent.horizontalCenter
            source: "./Images/base.png"
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 20
            Behavior on y {
                NumberAnimation { easing.overshoot: 1.802; duration: closebase.running?770:1300; easing.type: Easing.InOutBack }
            }

            Timer{
                id:closebase
                interval:6000
                onTriggered: base.y=480
            }
            Bigbutton {
                id: bigbutton
                play:false  //property of the bool type
                onPlayChanged: {
                    if (play) {
                        player.play()
                        player.seek(18000*2)
                    } else
                        player.pause()
                }

                x: 11
                y: 10
            }
            Smallbuton {
                id: smallbuton
                x: 76
                y: 16
                onStop: {bigbutton.play=false
                    closebase.restart()
                }      //stop sgnal for clicked on the stop icon
            }
            Slider { //you will have to do the time conversion into the pixel slider position position varies fom 0 to (150-7)
                id: sliderVideo
                x: 138
                y: 35
                width:151
                position: 0      //you can chage this value to move the slider from the outside but you can ask its position like any other property
                onClik: {closebase.restart()}  //signal for the slider chaged after duguguing
                debugint: 450    //time that slider takes to admit a true final value if not set =450miliseconds
                onPositionChanged: closebase.restart()

            }
            TextWidthShadow {
                id: time
                y: 47
                anchors.horizontalCenter: sliderVideo.horizontalCenter
                textstrig: Math.floor(sliderVideo.position)
            }

            Slider {
                id: sliderAudio
                x: 312
                y: 35
                width:62
                position: 50      //you can chage this value to move the slider from the outside but you can ask its position like any other property
                onClik: {closebase.restart()}  //signal for the slider chaged after duguguing
                debugint: 450    //time that slider takes to admit a true final value if not set =450miliseconds
                onPositionChanged: closebase.restart()
            }
            TextWidthShadow {
                id: volume
                y: 47
                anchors.horizontalCenterOffset: -2
                anchors.horizontalCenter: sliderAudio.horizontalCenter
                textstrig: Math.round(sliderAudio.position/(sliderAudio.width-7)*100)+"%"
            }
        }
    }
}

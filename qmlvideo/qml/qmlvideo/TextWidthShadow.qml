import QtQuick 2.0

Item{
    id:textWidthShadow
    property string textstrig: "value"
    width:text1copy.width
    height: text1copy.height
    Text {
        id: text1copy
        x: 1  
        color: "#ffffff"
        text: parent.textstrig
        styleColor: "#000000"
        opacity: 0.4
        style: Text.Outline
        font.pixelSize: 13
    }
    
    Text {
        id: text1
        color: "#ffffff"
        text: parent.textstrig
        font.pixelSize: 13
    }
}

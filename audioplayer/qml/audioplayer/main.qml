import QtQuick 2.0
import QtQuick.Controls 1.0

Rectangle {
    width: 256
    height: 128
    color: "#1F2050"

    Item {
        id: sliderContainer
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: buttonRow.top
        anchors.margins: 6

        Item {
            id: volumeSliderContainer
            anchors.top: parent.top
            anchors.bottom: slider.top
            anchors.right: parent.right
            anchors.bottomMargin: 6
            width: icon.width

            Image {
                id: icon
                anchors.top: parent.top
                source: "images/vol_speaker.png"
            }

            Rectangle {
                id: volumeSlider
                anchors.top: icon.bottom
                anchors.topMargin: 6
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom: parent.bottom
                width: 6

                border.color: "#80A0FF"
                border.width: 1
                color: "transparent"

                Rectangle {
                    id: volumeSliderFill
                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.bottom: parent.bottom
                    color: "#80A0FF"
                    height: audio.volume * parent.height

                    function update(vol) {
                        console.debug(vol)
                        height = vol * parent.height
                    }

                    Component.onCompleted: update(audio.volume)
                }
            }
        }

        Rectangle {
            id: slider
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            height: 6

            border.color: "#80A0FF"
            border.width: 1
            color: "transparent"

            Rectangle {
                id: sliderFill
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.bottom: parent.bottom
                color: "#80A0FF"

                function __update(time, totalTime) {
                    var _nomProgress = time / totalTime
                    var _width = parent.width * _nomProgress
                    if (_width >= parent.width)
                        sliderFill.width = parent.width
                    else
                        sliderFill.width = _width
                }
            }

            function updateFill(time) {
                sliderFill.__update(time, player.totalTime)
            }
        }

        Item {
            id: container
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: slider.top
            anchors.bottomMargin: 6

            Text {
                id: metaDataLabel
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.right: parent.right

                text: qsTr("Title | Artist | Album | Length")
                color: "#897D89"
            }

            Text {
                id: metaDataText
                anchors.top: metaDataLabel.bottom
                anchors.margins: 6

                text: "Daft Punk | Get Lucky | Random Access ..."
                color: "white"
            }

            Text {
                id: timeText
                anchors.top: metaDataText.bottom
                anchors.margins: 6

                text: "xx:xx:xx"
                color: "white"
                font.pixelSize: font.pixelSize*1.5
                font.bold: true

                function __padIntString(number) {
                    var str = '' + number;
                    if (str.length < 2)
                        str = '0' + str;
                    return str;
                }

                function __parseTime(ms) {
                    if (ms < 0)
                        return "0"
                    // Do not display hour unless there is anything to show.
                    // Always show minutes and seconds though.
                    // Rationale: plenty of videos are <1h but >1m
                    var showHour = true
                    if (player.totalTime < 3600000)
                        showHour = false

                    var s = Math.floor(ms / 1000)
                    var m = Math.floor( s / 60)
                    if (showHour)
                        var h = Math.floor(m / 60)

                    var time = '';
                    time = __padIntString(s % 60)
                    time = __padIntString(m % 60) + ':' + time
                    if (showHour)
                        // Do not pad hour as it looks ugly, also hour can exceed 24 anyway.
                        time = h + ':' + time
                    return time
                }

                function updateText(time) {
                    text = __parseTime(time)
                }
            }

            Item {
                id: windowButtonRow
                anchors.bottom: parent.bottom
                height: eqButton.height

                Image{
                    id: eqButton
                    anchors.left: parent.left
                    source: "images/eq_inactive2.png"
                }

                Image{
                    anchors.left: eqButton.right
                    anchors.margins: 6
                    source: "images/pl_inactive2.png"
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            console.debug("click")
                            var component = Qt.createComponent("playlist.qml");
                            console.debug("")
//                            if (component.status == Component.Ready) {
                                var button = component.createObject(container);
                            button.width = 640
                            button.height = 480
                            button.visible = true
//                                button.color = "red";

//                            }
                        }
                    }
                }
            }

            Component.onCompleted: {
                player.setTickInterval(100)
                player.timeChanged.connect(timeText.updateText)
                player.timeChanged.connect(slider.updateFill)
                audio.volumeChanged.connect(volumeSliderFill.update)
            }
        }
    }

    Row {
        id: buttonRow
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        height: 22

        Button {
            width: parent.width / 5
            height: parent.height
            iconSource: "images/b_prev.png"
        }

        Button {
            width: parent.width / 5
            height: parent.height
            iconSource: "images/b_play.png"
            onClicked: player.play()
        }

        Button {
            width: parent.width / 5
            height: parent.height
            iconSource: "images/b_pause.png"
            onClicked: player.pause()
        }

        Button {
            width: parent.width / 5
            height: parent.height
            iconSource: "images/b_stop.png"
            onClicked: player.stop()
        }

        Button {
            width: parent.width / 5
            height: parent.height
            iconSource: "images/b_next.png"
        }
    }
}

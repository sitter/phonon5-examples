import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Dialogs 1.0
import QtQuick.Layouts 1.0
import org.kde.phonon 1.0

ApplicationWindow {
    width: 640
    height: 480
    visible: true

    FileDialog {
        id: fileDialog
        title: "Please choose a file"
        folder: "/home/me/Videos"
        onAccepted: {
            player.stop()
            player.url = fileUrl
            visible = false
            player.setTickInterval(100)
            player.play()
        }
    }

    Rectangle {
        anchors.fill: parent
        color: "black"

        Player {
            id: player
        }

        Audio {
            id: audio
            Component.onCompleted: player.addOutput(audio)
        }

        Video {
            id: video
            anchors.fill: parent
            Component.onCompleted: player.addOutput(video)

            focus: true
            Keys.onReleased: {
                if (event.key === Qt.Key_Left)
                    player.seek(player.time - 5*1000)
                if (event.key === Qt.Key_Right)
                    player.seek(player.time + 5*1000)
                if (event.key === Qt.Key_F)
                    fullscreener.fullScreen = !fullscreener.fullScreen
            }

            MouseArea {
                anchors.fill: parent
                onDoubleClicked: fullscreener.fullScreen = !fullscreener.fullScreen
            }
        }
    }

    ToolBar {
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        opacity: 0.9
        RowLayout {
            anchors.fill: parent
            opacity: 1

            ToolButton {
                id: playButton
                iconName: player.playing ? "media-playback-pause" : "media-playback-start"
                iconSource: player.playing ? "/usr/share/icons/oxygen/32x32/actions/media-playback-pause.png"
                                           : "/usr/share/icons/oxygen/32x32/actions/media-playback-start.png"
                onClicked: {
                    console.debug(player.url)
                    if (!player.stopped) {
                        if (player.playing)
                            player.pause()
                        else
                            player.play()
                    } else {
                        fileDialog.open()
                    }
                }
            }

            Slider {
                Layout.fillWidth: true
                value: player.time
                minimumValue: 0
                maximumValue: player.totalTime
                onValueChanged: {
                    if (pressed)
                        player.seek(value)
                }
            }

            ToolButton {
                id: fullscreenButton
                iconName: "view-fullscreen"
                iconSource: "/usr/share/icons/oxygen/32x32/actions/view-fullscreen.png"
                onClicked: fullscreener.fullScreen = !fullscreener.fullScreen
            }
        }
    }
}

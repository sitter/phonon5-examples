#include <QApplication>
#include <QQmlApplicationEngine>

#include <QtQml>
#include <QQmlContext>
#include <QtCore/QStandardPaths>
#include <QWindow>

#include <phonon/audiooutput.h>
#include <phonon/player.h>
#include <phonon/source.h>
#include <phonon/videoitem.h>

using namespace Phonon;

class Fullscreener : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool fullScreen READ isFullScreen WRITE setFullScreen NOTIFY fullScreenChanged)
public:
    Fullscreener(QObject *parent = 0)
        : QObject(parent)
        , m_fullScreen(false)
    {
    }

    bool isFullScreen() const { return m_fullScreen; }

    void setFullScreen(bool fullScreen)
    {
        if (fullScreen) {
            m_fullScreen = true;
            qApp->focusWindow()->showFullScreen();
        } else {
            m_fullScreen = false;
            qApp->focusWindow()->showNormal();
        }
        emit fullScreenChanged();
    }

signals:
    void fullScreenChanged();

private:
    bool m_fullScreen;
};

static inline QString qmlPath(const QString &file)
{
    return QStandardPaths::locate(QStandardPaths::DataLocation, QString("qml/%1").arg(file));
}

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    qmlRegisterType<Player>("org.kde.phonon", 1, 0, "Player");
    qmlRegisterType<AudioOutput>("org.kde.phonon", 1, 0, "Audio");
    qmlRegisterType<VideoItem>("org.kde.phonon", 1, 0, "Video");

    QQmlApplicationEngine engine;
    Fullscreener fullscreener;
    engine.rootContext()->setContextProperty("fullscreener", &fullscreener);
    engine.load(qmlPath(QLatin1String("main.qml")));

    return app.exec();
}

#include "main.moc"

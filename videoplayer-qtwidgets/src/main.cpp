#include <QApplication>

#include "MainWindow.h"

int main(int argc, char **argv)
{
    setenv("PHONON_DEBUG", "5", 1);
    setenv("PHONON_BACKEND_DEBUG", "5", 1);
    setenv("PHONON_SUBSYSTEM_DEBUG", "5", 1);
    setenv("PHONON_PULSEAUDIO_DEBUG", "5", 1);

    QApplication app(argc, argv);

    app.setOrganizationName(QLatin1Literal("KDE"));
    app.setOrganizationDomain(QLatin1Literal("kde.org"));

    app.setApplicationName(QLatin1Literal("Video Player"));
    app.setApplicationVersion(QLatin1Literal("1.0.0"));
    app.setApplicationDisplayName(QObject::tr("Video Player"));

    MainWindow w;
    w.show();

    return app.exec();
}

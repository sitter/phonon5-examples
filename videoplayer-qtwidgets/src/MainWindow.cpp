#include "MainWindow.h"
#include "ui_MainWindow.h"

#include <QDebug>
#include <QFileDialog>
#include <QStandardPaths>
#include <QUrl>

#include <phonon/audiooutput.h>
#include <phonon/player.h>
#include <phonon/source.h>

using namespace Phonon;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , m_audioOutput(0)
    , m_player(0)
{
    ui->setupUi(this);
    QMetaObject::invokeMethod(this, "init"); // lazy init
//    Player::play(QUrl("file:///home/me/Music/Gossip - Move In The Right Direction.mp3"));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::init()
{
    m_audioOutput = new AudioOutput(this);

    m_player = new Player(this);
    m_player->setTickInterval(1);
    m_player->addOutput(m_audioOutput);
    m_player->addOutput(ui->videoWidget);

    connect(ui->loadButton, SIGNAL(clicked()), this, SLOT(load()));
    connect(ui->seekBar, SIGNAL(sliderMoved(int)), this, SLOT(seek(int)));

    connect(ui->playButton, SIGNAL(clicked()), m_player, SLOT(play()));
    connect(ui->pauseButton, SIGNAL(clicked()), m_player, SLOT(pause()));
    connect(ui->stopButton, SIGNAL(clicked()), m_player, SLOT(stop()));

    connect(m_player, SIGNAL(seekableChanged(bool)), this, SLOT(onSeekableChanged(bool)));
    connect(m_player, SIGNAL(timeChanged(qint64)), this, SLOT(onTimeChanged(qint64)));
    connect(m_player, SIGNAL(totalTimeChanged(qint64)), this, SLOT(onTotalTimeChanged(qint64)));
}

void MainWindow::load()
{
    const QString moviesDir = QStandardPaths::standardLocations(QStandardPaths::MoviesLocation).at(0);
    const QString file = QFileDialog::getOpenFileName(this,
                                                      QString(),
                                                      moviesDir);
    const QUrl url = QUrl::fromLocalFile(file);
    if (url.isEmpty() || !url.isValid())
        return;
    m_player->setSource(Source(url));
    ui->statusbar->showMessage(file);
}

void MainWindow::seek(int time)
{
    m_player->seek(time);
}

void MainWindow::onSeekableChanged(bool seekable)
{
    ui->seekBar->setEnabled(seekable);
}

void MainWindow::onTimeChanged(qint64 time)
{
    ui->seekBar->setValue(time);
}

void MainWindow::onTotalTimeChanged(qint64 length)
{
    ui->seekBar->setMaximum(length);
}

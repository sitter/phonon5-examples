set(srcs
    main.cpp
    MainWindow.cpp
)

qt5_wrap_ui(srcs
    MainWindow.ui
)

add_executable(videoplayer ${srcs})
qt5_use_modules(videoplayer Core Widgets)

target_link_libraries(videoplayer ${PHONON_LIBRARY})

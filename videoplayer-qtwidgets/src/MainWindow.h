#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Phonon {
class AudioOutput;
class Player;
}

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void init();
    void load();

    void seek(int time);
    void onSeekableChanged(bool seekable);
    void onTimeChanged(qint64 time);
    void onTotalTimeChanged(qint64 length);
    
private:
    Ui::MainWindow *ui;
    Phonon::AudioOutput *m_audioOutput;
    Phonon::Player *m_player;
};

#endif // MAINWINDOW_H
